import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Bolo } from './home/bolo';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CarrinhoService {

  constructor(private afs: AngularFirestore) { }

  getCarrinho(): Observable<Bolo[]> {
    return this.afs.collection<Bolo>('carrinho').snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  adcionarCarrinho(bolo: Bolo) {
    return this.afs.collection<Bolo>('carrinho').doc(bolo['id'].toString()).set(bolo);
  }

  removerCarrinho(id: string) {
    return this.afs.collection<Bolo>('carrinho').doc(id).delete();
  }

  finalizarCompra(bolos: Bolo[]) {
    bolos.forEach(bolo => {
      this.afs.collection<Bolo>('carrinho').doc(bolo['id'].toString()).delete();
    });
  }
}
