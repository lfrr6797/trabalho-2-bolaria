import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bolo } from './home/bolo';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BoloFirebaseService {

  bolos: Bolo[];

  constructor(private http: HttpClient, private afs: AngularFirestore) { }

  getBolos(): Observable<Bolo[]> {
    return this.afs.collection<Bolo>('bolo').snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const id = a.payload.doc.id;
          const data = a.payload.doc.data();
          return { id, ...data };
        })
      })
    )
  }

}
