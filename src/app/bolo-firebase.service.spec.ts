import { TestBed } from '@angular/core/testing';

import { BoloFirebaseService } from './bolo-firebase.service';

describe('BoloFirebaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BoloFirebaseService = TestBed.get(BoloFirebaseService);
    expect(service).toBeTruthy();
  });
});
