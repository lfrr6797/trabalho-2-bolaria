// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCgeN7AqE-rV4IAJI04M4XLyW3oEcxC5E4",
    authDomain: "bolaria-teo.firebaseapp.com",
    databaseURL: "https://bolaria-teo.firebaseio.com",
    projectId: "bolaria-teo",
    storageBucket: "bolaria-teo.appspot.com",
    messagingSenderId: "655864827984",
    appId: "1:655864827984:web:d3485977d8b40147764155"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
